/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poligonosregulares;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Henrique Maciel <1150863@isep.ipp.pt>
 */
public class PoligonosRegularesTest {

    PoligonosRegulares poligonos = new PoligonosRegulares();
    LerFicheiro leitura = new LerFicheiro();

    private final String fichUnidades = "poligonos_prefixo_unidades.txt";
    private final String fichDezenas = "poligonos_prefixo_dezenas.txt";
    private final String fichCentenas = "poligonos_prefixo_centenas.txt";

    private final String fichLados = "teste_lados_nome.txt";
    private final String fichNome = "teste_nome_lados.txt";

    public PoligonosRegularesTest() throws FileNotFoundException {
        leitura.lerFicheiroUnidades(fichUnidades);
        leitura.lerFicheiroDezenas(fichDezenas);
        leitura.lerFicheiroCentenas(fichCentenas);
    }

    //Alinea b
    @Test
    public void testGetNomePoligono() {
        System.out.println("NomePoligonoTest");
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fichLados));
            String linha;
            while ((linha = reader.readLine()) != null) {
                String[] tmp = linha.split(";");
                String expected = tmp[1];
                assertEquals(expected, poligonos.getNomePoligono(Integer.parseInt(tmp[0])));
            }
            reader.close();
        } catch (Exception e) {
            System.err.format("Exception occurred trying to read '%s'.", fichLados);
            e.printStackTrace();
        }
    }

    //Alinea c
    @Test
    public void testCriarArvoreMae() {
        System.out.println("Criação da árvore mãe:");
        poligonos.criarArvoreMae();
        System.out.println(poligonos.getTreeMae().toString());
        assertTrue("A árvore mãe tem que estar preenchida", poligonos.getTreeMae().size() > 0);
    }

    //Alinea d
    @Test
    public void testGetLadosPoligono() {
        System.out.println("LadosPoligonoTest");
        poligonos.criarArvoreMae();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fichNome));
            String linha;
            while ((linha = reader.readLine()) != null) {
                String[] tmp = linha.split(";");
                int expected = Integer.parseInt(tmp[1]);
                assertEquals(expected, poligonos.getLadosPoligono(tmp[0]));
            }
            reader.close();
        } catch (Exception e) {
            System.err.format("Exception occurred trying to read '%s'.", fichNome);
            e.printStackTrace();
        }
    }

    //Alinea e
    @Test
    public void testGetIntervaloNumLados() {
        System.out.println("Obter Lista de Poligonos de um determinado intervalo do maior para o menor:");
        System.out.println("Intervalo de 1 a 9");
        int i = 0;
        String[] list = {"enneagon", "octagon", "heptagon", "hexagon", "pentagon", "tetragon", "trigon", "digon", "henagon"};
        for (String s : poligonos.getIntervaloNumLados(1, 9)) {
            System.out.println(list[i]);
            assertEquals(list[i], s);
            i++;
        }
    }

    //Alinea f
    @Test
    public void testLowestCommonAncestor() {
        System.out.println("");
        System.out.print("Antecessor comum mais próximo de ");
        poligonos.criarArvoreMae();
        String expected = "pentahectadodecagon";
        System.out.println("Henagon e Enneahectaenneacontaenneagon é:");
        assertEquals(expected, poligonos.lowestCommonAncestor("henagon", "enneahectaenneacontaenneagon"));
        System.out.println(poligonos.lowestCommonAncestor("henagon", "enneahectaenneacontaenneagon"));
    }

}
