/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poligonosregulares;

import java.io.FileNotFoundException;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author Henrique Maciel <1150863@isep.ipp.pt>
 */
public class LerFicheiroTest {

    LerFicheiro leitura = new LerFicheiro();
    PoligonosRegulares poligonos = new PoligonosRegulares();

    private final String fichUnidades = "poligonos_prefixo_unidades.txt";
    private final String fichDezenas = "poligonos_prefixo_dezenas.txt";
    private final String fichCentenas = "poligonos_prefixo_centenas.txt";

    /**
     * Test of lerFicheiroUnidades method, of class LerFicheiro.
     *
     * @throws java.io.FileNotFoundException
     */
    @Test
    public void testLerFicheiroUnidades() throws FileNotFoundException {
        System.out.println("UNIDADES");
        int expected = 1;
        leitura.lerFicheiroUnidades(fichUnidades);
        System.out.println(poligonos.getTreeUnidades().toString());
        assertTrue("A árvore das unidades deve conter elementos", poligonos.getTreeUnidades().size() > 0);
        assertEquals(expected, poligonos.getTreeUnidades().smallestElement().getNumLados());
    }

    /**
     * Test of lerFicheiroDezenas method, of class LerFicheiro.
     *
     * @throws java.io.FileNotFoundException
     */
    @Test
    public void testLerFicheiroDezenas() throws FileNotFoundException {
        System.out.println("DEZENAS");
        int expected = 10;
        leitura.lerFicheiroDezenas(fichDezenas);
        System.out.println(poligonos.getTreeDezenas().toString());
        assertTrue("A árvore das dezenas deve conter elementos", poligonos.getTreeDezenas().size() > 0);
        assertEquals(expected, poligonos.getTreeDezenas().smallestElement().getNumLados());

    }

    /**
     * Test of lerFicheiroCentenas method, of class LerFicheiro.
     *
     * @throws java.io.FileNotFoundException
     */
    @Test
    public void testLerFicheiroCentenas() throws FileNotFoundException {
        System.out.println("CENTENAS");
        int expected = 100;
        leitura.lerFicheiroCentenas(fichCentenas);
        System.out.println(poligonos.getTreeCentenas().toString());
        assertTrue("A árvore das centenas deve conter elementos", poligonos.getTreeCentenas().size() > 0);
        assertEquals(expected, poligonos.getTreeCentenas().smallestElement().getNumLados());

    }

}
