package PL;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Stack;

/*
 * @author DEI-ESINF
 * @param <E>
 */
public class TREE<E extends Comparable<E>> extends BST<E> {

    /*
   * @param element A valid element within the tree
   * @return true if the element exists in tree false otherwise
     */
    public boolean contains(E element) {
        return (find(element, root) != null);
    }

    public boolean isLeaf(E element) {
        Node<E> node = find(element, root);
        return (node == null) ? false : (node.getLeft() == null && node.getRight() == null);
    }

    /*
   * build a list with all elements of the tree. The elements in the 
   * left subtree in ascending order and the elements in the right subtree 
   * in descending order. 
   *
   * @return    returns a list with the elements of the left subtree 
   * in ascending order and the elements in the right subtree is descending order.
     */
    public Iterable<E> ascdes() {
        List<E> result = new ArrayList<>();
        if (root == null) {
            return result;
        }
        inOrder(root.getLeft(), result);
        result.add(root.getElement());
        List<E> right = new ArrayList<>();
        inOrder(root.getRight(), right);
        Collections.reverse(right);
        result.addAll(right);
        return result;
    }

    private void inOrder(Node<E> node, List<E> snapshot) {
        if (node == null) {
            return;
        }
        inOrder(node.getLeft(), snapshot);
        snapshot.add(node.getElement());
        inOrder(node.getRight(), snapshot);
    }

    private void ascSubtree(Node<E> node, List<E> snapshot) {
        if (node == null) {
            return;
        }
        ascSubtree(node.getLeft(), snapshot);
        snapshot.add(node.getElement());
        ascSubtree(node.getRight(), snapshot);
    }

    private void desSubtree(Node<E> node, List<E> snapshot) {
        if (node == null) {
            return;
        }
        desSubtree(node.getRight(), snapshot);
        snapshot.add(node.getElement());
        desSubtree(node.getLeft(), snapshot);
    }

    /**
     * Returns the tree without leaves.
     *
     * @return tree without leaves
     */
    public BST<E> autumnTree() {
        BST<E> newTree = new TREE();
        newTree.root = copyRec(root);
        return newTree;
    }

    private Node<E> copyRec(Node<E> node) {
        if ((node == null) || isLeafNode(node)) {
            return null;
        }
        return new Node(node.getElement(), copyRec(node.getLeft()), copyRec(node.getRight()));
    }

    private boolean isLeafNode(Node<E> node) {
        return (node == null) ? false : (node.getLeft() == null && node.getRight() == null);
    }

    /**
     * @return the the number of nodes by level.
     */
    public int[] numNodesByLevel() {
        int[] a = new int[this.height()];
        numNodesByLevel(root, a, 0);
        return a;
    }

    private int numNodesByLevel(Node<E> node, int[] result, int level) {
        if (node == null) {
            return 0;
        }

        if (result[level] == level) {
            return 1;
        }

        return numNodesByLevel(node.getLeft(), result, level + 1)
                + numNodesByLevel(node.getRight(), result, level + 1);
    }

}
