/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poligonosregulares;

import PL.AVL;
import java.util.ArrayList;

/**
 *
 * @author Henrique Maciel <1150863@isep.ipp.pt>
 */
public class PoligonosRegulares extends AVL<Poligono> {

    private static AVL<Poligono> treeUnidades;
    private static AVL<Poligono> treeDezenas;
    private static AVL<Poligono> treeCentenas;

    private static AVL<Poligono> treeMae;

    public PoligonosRegulares() {
        treeUnidades = new AVL();
        treeDezenas = new AVL();
        treeCentenas = new AVL();
        treeMae = new AVL();
    }

    public AVL<Poligono> getTreeUnidades() {
        return treeUnidades;
    }

    public void setTreeUnidades(AVL<Poligono> treeUnidades) {
        this.treeUnidades = treeUnidades;
    }

    public AVL<Poligono> getTreeDezenas() {
        return treeDezenas;
    }

    public void setTreeDezenas(AVL<Poligono> treeDezenas) {
        this.treeDezenas = treeDezenas;
    }

    public AVL<Poligono> getTreeCentenas() {
        return treeCentenas;
    }

    public void setTreeCentenas(AVL<Poligono> treeCentenas) {
        this.treeCentenas = treeCentenas;
    }

    public AVL<Poligono> getTreeMae() {
        return treeMae;
    }

    public void setTreeMae(AVL<Poligono> treeMae) {
        PoligonosRegulares.treeMae = treeMae;
    }

    //Alinea b
    public String getNomePoligono(int numLados) {
        StringBuilder stringBuilder = new StringBuilder();
        String num = Integer.toString(numLados);
        Poligono poligono;

        if (num.length() < 0 || num.length() > 3) {
            return null;
        }

        if (num.length() == 1) {
            poligono = getNode(Integer.parseInt(num), treeUnidades);
            stringBuilder.append(poligono.getDesignacao());
        } else {
            while (num.length() > 1) {
                if (num.length() == 3) {
                    int numOrdem = Character.getNumericValue(num.charAt(0)) * 100;
                    poligono = getNode(numOrdem, treeCentenas);
                    stringBuilder.append(poligono.getDesignacao());
                }
                if (num.length() == 2 && Character.getNumericValue(num.charAt(0)) != 0) {
                    if (Integer.parseInt(num) > 30) {
                        int numOrdem = Character.getNumericValue(num.charAt(0)) * 10;
                        poligono = getNode(numOrdem, treeDezenas);
                        stringBuilder.append(poligono.getDesignacao());
                        numOrdem = Character.getNumericValue(num.charAt(1));
                        poligono = getNode(numOrdem, treeUnidades);
                        stringBuilder.append(poligono.getDesignacao());
                    } else {
                        poligono = getNode(Integer.parseInt(num), treeDezenas);
                        stringBuilder.append(poligono.getDesignacao());
                    }
                }
                if (num.length() == 2 && Character.getNumericValue(num.charAt(0)) == 0) {
                    num = num.substring(1);
                    poligono = getNode(Integer.parseInt(num), treeUnidades);
                    stringBuilder.append(poligono.getDesignacao());
                }
                num = num.substring(1);
            }
        }

        stringBuilder.append("gon");
        String designacao = stringBuilder.toString();
        return designacao;
    }

    //Alinea b
    private Poligono getNode(int num, AVL<Poligono> tree) {
        Poligono ret = new Poligono();

        for (Poligono p : tree.inOrder()) {
            if (num == p.getNumLados()) {
                ret = p;
            }
        }
        return ret;
    }

    //Alinea c
    public void criarArvoreMae() {
        for (int i = 1; i < 1000; i++) {
            String designacao = getNomePoligono(i);
            Poligono poligono = new Poligono(i, designacao);
            treeMae.insert(poligono);
        }
    }

    //Alinea d
    public int getLadosPoligono(String designacao) {
        Poligono poligono = getNodeDesignacao(designacao, treeMae);
        return poligono.getNumLados();
    }

    //Alinea d
    private Poligono getNodeDesignacao(String designacao, AVL<Poligono> tree) {
        Poligono ret = new Poligono();

        for (Poligono p : tree.inOrder()) {
            if (designacao.equals(p.getDesignacao())) {
                ret = p;
            }
        }
        return ret;
    }

    //Alinea e
    public ArrayList<String> getIntervaloNumLados(int limInferior, int limSuperior) {
        ArrayList<String> lista = new ArrayList();

        for (int i = limSuperior; i >= limInferior; i--) {
            lista.add(getNomePoligono(i));
        }

        return lista;
    }

    //Alinea f
    public String lowestCommonAncestor(String designacao1, String designacao2) {
        Poligono p1 = new Poligono(getLadosPoligono(designacao1), designacao1);
        Poligono p2 = new Poligono(getLadosPoligono(designacao2), designacao2);

        return treeMae.lowestCommonAncestor(p1, p2).getElement().getDesignacao();
    }

}
