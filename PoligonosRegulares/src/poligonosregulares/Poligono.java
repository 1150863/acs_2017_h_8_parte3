/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poligonosregulares;

/**
 *
 * @author Henrique Maciel <1150863@isep.ipp.pt>
 */
public class Poligono implements Comparable<Poligono> {

    private int numLados;
    private String designacao;

    private final int NUM_LADOS_POR_OMISSAO = 0;
    private final String DESIGNACAO_POR_OMISSAO = "";

    public Poligono(int numLados, String designacao) {
        this.numLados = numLados;
        this.designacao = designacao;
    }

    public Poligono() {
        this.numLados = NUM_LADOS_POR_OMISSAO;
        this.designacao = DESIGNACAO_POR_OMISSAO;
    }

    public int getNumLados() {
        return numLados;
    }

    public void setNumLados(int numLados) {
        this.numLados = numLados;
    }

    public String getDesignacao() {
        return designacao;
    }

    public void setDesignacao(String designacao) {
        this.designacao = designacao;
    }

    @Override
    public int compareTo(Poligono p) {
        if (numLados > p.getNumLados()) {
            return 1;
        } else if (numLados < p.getNumLados()) {
            return -1;
        } else {
            return 0;
        }
    }

    @Override
    public String toString() {
        return "Poligono{" + "numLados=" + numLados + ", designacao=" + designacao + '}';
    }

}
