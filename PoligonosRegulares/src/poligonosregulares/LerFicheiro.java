/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poligonosregulares;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 *
 * @author Henrique Maciel <1150863@isep.ipp.pt>
 */
public class LerFicheiro {

    PoligonosRegulares poligReg = new PoligonosRegulares();

    //Alinea a
    public void lerFicheiroUnidades(String fileName) throws FileNotFoundException {

        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            String linha;
            while ((linha = reader.readLine()) != null) {
                String[] tmp = linha.split(";");
                if (tmp[0].length() == 1) {
                    Poligono poligono = new Poligono(Integer.parseInt(tmp[0]), tmp[1]);
                    poligReg.getTreeUnidades().insert(poligono);
                } else {
                    throw new IllegalArgumentException("Poligono deve ter lados na ordem das unidades!");
                }
            }
            reader.close();
        } catch (Exception e) {
            System.err.format("Exception occurred trying to read '%s'.", fileName);
            e.printStackTrace();
        }
    }

    //Alinea a
    public void lerFicheiroDezenas(String fileName) throws FileNotFoundException {

        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            String linha;
            while ((linha = reader.readLine()) != null) {
                String[] tmp = linha.split(";");
                if (tmp[0].length() == 2) {
                    Poligono poligono = new Poligono(Integer.parseInt(tmp[0]), tmp[1]);
                    poligReg.getTreeDezenas().insert(poligono);
                } else {
                    throw new IllegalArgumentException("Poligono deve ter lados na ordem das dezenas!");
                }
            }
            reader.close();
        } catch (Exception e) {
            System.err.format("Exception occurred trying to read '%s'.", fileName);
            e.printStackTrace();
        }
    }

    //Alinea a
    public void lerFicheiroCentenas(String fileName) throws FileNotFoundException {

        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            String linha;
            while ((linha = reader.readLine()) != null) {
                String[] tmp = linha.split(";");
                if (tmp[0].length() == 3) {
                    Poligono poligono = new Poligono(Integer.parseInt(tmp[0]), tmp[1]);
                    poligReg.getTreeCentenas().insert(poligono);
                } else {
                    throw new IllegalArgumentException("Poligono deve ter lados na ordem das centenas!");
                }
            }
            reader.close();
        } catch (Exception e) {
            System.err.format("Exception occurred trying to read '%s'.", fileName);
            e.printStackTrace();
        }
    }
}
